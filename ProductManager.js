
const { promises } = require('fs')
const fs = promises

let productos = []

class ProductManager {
    constructor () {
        this.products = productos 
        this.path = './data.json'
        
    }

    addProduct(newProduct){

        if (!newProduct.title ||
            !newProduct.description ||
            !newProduct.price ||
            !newProduct.thumbmail ||
            !newProduct.code ||
            !newProduct.stock) return "todos los campos son necesarios"
        
        let product = this.products.find(prod => prod.code == newProduct.code)
        if (product) return "Un producto con este codigo ya fue ingresado"

        if(this.products.length == 0){
            return this.products.push({id: 1, ...newProduct})
        }

        return [...this.products, {id: this.products[this.products.length-1].id + 1 , ...newProduct}]
    }

    getProducts(){
      consultarProducto = async () => {
        if (fs.existsSync(path)) {
            const data = await fs.promises.readFile(path, 'utf-8');
            console.log(data);
            const products = JSON.parse(data);
            return products;
        }
        else{
            return [] 
        }
    }
    crearProduct = async (product) => {
        const products =  await this.consultarProduct();
        if(products.length===0){
            product.id=1;
        }else{
            product.id = products[products.length-1].id+1;
        }
        products.push(product);
        await fs.promises.writeFile(path,JSON.stringify(products,null,'\t'));
        return product;
    }
    }

    getProductById(id){
        let product = this.products.find(prod => prod.id == id)
        if (!product) return "Product Not Found"
        return product
    }
}

const product = new ProductManager()

product.addProduct({
    title: "Producto 1",
    description: "Esto es una descripcion",
    price: 1500,
    thumbmail: "link",
    code: 001,
    stock: 100
})
cpnsole.log(product.addProduct({
    title: "Producto 2",
    description: "Esto es una descripcion",
    price: 1500,
    thumbmail: "link",
    code: 002,
    stock: 100
}))

console.log(product.getProducts());
